defmodule SecretSanta do
  @moduledoc """
  SecretSanta application used to create and manage lists persisted in Redis.
  """
  def redis_key do
    Application.get_env(:secret_santa, :redis_key)
  end
end
