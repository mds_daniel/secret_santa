defmodule SecretSanta.CLI do
  @moduledoc """
  Module implementing the SecretSanta CLI escript
  """
  use SecretSanta.CLICommands
  alias Optimus.ParseResult

  cli_command :add, SecretSanta.CLI.Add
  cli_command :remove, SecretSanta.CLI.Remove
  cli_command :list, SecretSanta.CLI.List
  cli_command :pairup, SecretSanta.CLI.PairUp
  cli_command :latest, SecretSanta.CLI.Latest
  cli_command :santa, SecretSanta.CLI.Santa
  cli_command :wish, SecretSanta.CLI.Wish
  cli_command :wishlist, SecretSanta.CLI.WishList

  def main(args) do
    cli_options()
    |> Optimus.new!()
    |> Optimus.parse!(args)
    |> run!()
  end

  def cli_options do
    [
      name: "secret_santa",
      description: "CLI for managing secret santa lists",
      version: "0.1.0",
      allow_unknown_args: false,
      subcommand: :help,
      subcommands: commands()
    ]
  end

  def run!({[command], opts}) do
    run_command!(command, command_options(opts))
  end

  def run!(_) do
    help_message()
  end

  def help_message do
    cli_options()
    |> Optimus.new!()
    |> Optimus.help()
    |> IO.puts()
  end

  defp command_options(%ParseResult{args: args, flags: flags, options: options}) do
    Enum.reduce([args, flags, options], [], fn opts, acc ->
      Keyword.merge(Enum.to_list(opts), acc)
    end)
  end
end
