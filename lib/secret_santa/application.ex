defmodule SecretSanta.Application do
  use Application

  def start(_type, _args) do
    SecretSanta.Supervisor.start_link([])
  end
end
