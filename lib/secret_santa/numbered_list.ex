defmodule SecretSanta.NumberedList do
  def display(items, padding \\ "  ") do
    each_item(items, fn row ->
      IO.puts(padding <> row)
    end)
  end

  def each_item(items, fun) do
    items
    |> Stream.with_index(1)
    |> Stream.map(&pad_item/1)
    |> Enum.each(fun)
  end

  def pad_item({item, index}) do
    "#{pad_index(index)}. #{item}"
  end

  def pad_index(index) do
    String.pad_leading(to_string(index), 2, " ")
  end
end
