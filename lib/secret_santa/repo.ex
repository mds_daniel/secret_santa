defmodule SecretSanta.Repo do
  import SecretSanta, only: [redis_key: 0]
  alias SecretSanta.{Pairing, Recipient}

  def people_key do
    "#{redis_key()}:people"
  end

  def latest_pairing_key do
    "#{redis_key()}:latest"
  end

  def wishlist_key(person) do
    "#{redis_key()}:wishlist:#{person}"
  end

  def add_person(person) do
    Redix.command(:redix, ["SADD", people_key(), person])
  end

  def remove_person(person) do
    Redix.command(:redix, ["SREM", people_key(), person])
  end

  def list_people do
    Redix.command(:redix, ["SMEMBERS", people_key()])
  end

  def persist_pairing(pairing) do
    Redix.command(:redix, ["HMSET", latest_pairing_key() | Pairing.flatten(pairing)])
  end

  def latest_pairing do
    Redix.command(:redix, ["HGETALL", latest_pairing_key()])
    |> build_pairing()
  end

  def delete_pairing do
    Redix.command(:redix, ["DEL", latest_pairing_key()])
  end

  def recipient_only(santa) do
    Redix.command(:redix, ["HGET", latest_pairing_key(), santa])
  end

  def add_to_wishlist(person, item) do
    Redix.command(:redix, ["SADD", wishlist_key(person), item])
  end

  def wishlist(person) do
    Redix.command(:redix, ["SMEMBERS", wishlist_key(person)])
  end

  def recipient_for(santa) do
    with {:ok, person} <- recipient_only(santa),
         {:ok, wishlist} <- wishlist(person) do
      {:ok, %Recipient{name: person, wishlist: wishlist}}
    end
  end

  defp build_pairing({:ok, keys_with_values}) do
    pairing = Pairing.from_flattened(keys_with_values)
    {:ok, pairing}
  end

  defp build_pairing({:error, _} = err), do: err
end
