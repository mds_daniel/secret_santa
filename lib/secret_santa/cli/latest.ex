defmodule SecretSanta.CLI.Latest do
  alias SecretSanta.Repo

  def cli_options do
    [
      name: "latest",
      about: "Show latest pairing, for all people"
    ]
  end

  def call(_opts) do
    case Repo.latest_pairing() do
      {:ok, pairing} ->
        display_pairing(pairing)

      {:error, _} ->
        IO.puts(:stderr, "Failed to fetch pairing")
    end
  end

  def display_pairing(pairing) do
    if Enum.empty?(pairing) do
      display_empty_pairing()
    else
      display_filled_pairing(pairing)
    end
  end

  def display_empty_pairing do
    IO.puts("No pairing made. Use the pairup command to create a new pairing.")
  end

  def display_filled_pairing(pairing) do
    IO.puts("Pairing:")

    Enum.each(pairing, fn {giver, recipient} ->
      IO.puts("  \"#{giver}\" gets gift for \"#{recipient}\"")
    end)
  end
end
