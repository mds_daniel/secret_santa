defmodule SecretSanta.CLI.WishList do
  alias SecretSanta.{Repo, NumberedList}

  def cli_options do
    [
      name: "wishlist",
      about: "Show the given PERSON's wishlist",
      args: [
        person: [
          name: "person",
          value_name: "PERSON",
          help: "Person for whom the wishlist is requested",
          required: true
        ],
      ]
    ]
  end

  def call(opts) do
    person = Keyword.fetch!(opts, :person)

    case Repo.wishlist(person) do
      {:ok, wishlist} ->
        display_wishlist(wishlist)

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to retrieve wishlist")
    end
  end

  def display_wishlist([]) do
    IO.puts("The wishlist is currently empty.")
  end

  def display_wishlist([single_item]) do
    IO.puts("The person only wishes for \"#{single_item}\"")
  end

  def display_wishlist(items) do
    IO.puts("There are #{length(items)} items on the wishlist")
    NumberedList.display(items)
  end
end
