defmodule SecretSanta.CLI.Remove do
  alias SecretSanta.Repo

  def cli_options do
    [
      name: "remove",
      about: "Remove given person from the list",
      args: [
        person: [
          name: "person",
          value_name: "PERSON",
          help: "Person to be removed from the list",
          required: true
        ]
      ]
    ]
  end

  def call(opts) do
    person = Keyword.fetch!(opts, :person)

    case Repo.remove_person(person) do
      {:ok, _result} ->
        IO.puts("Removed person: \"#{person}\".")

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to remove person.")
    end
  end
end
