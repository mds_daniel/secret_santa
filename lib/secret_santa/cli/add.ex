defmodule SecretSanta.CLI.Add do
  alias SecretSanta.Repo

  def cli_options do
    [
      name: "add",
      about: "Add given person to the list",
      args: [
        person: [
          name: "person",
          value_name: "PERSON",
          help: "Person to be added to the list",
          required: true
        ]
      ]
    ]
  end

  def call(opts) do
    person = Keyword.fetch!(opts, :person)

    case Repo.add_person(person) do
      {:ok, _result} ->
        IO.puts("Added person: \"#{person}\".")

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to add person.")
    end
  end
end
