defmodule SecretSanta.CLI.PairUp do
  alias SecretSanta.{Repo, Pairing}

  def cli_options do
    [
      name: "pairup",
      about: "Pair each person to their secret santa",
    ]
  end

  def call(_opts) do
    Repo.list_people()
    |> make_pairing()
    |> persist()
    |> display_result()
  end

  def make_pairing({:ok, people}) do
    {:ok, Pairing.pair_up(people)}
  end

  def make_pairing({:error, _reason}) do
    IO.puts(:stderr, "Failed to fetch people list")
    :error
  end

  def persist({:ok, pairing}) do
    Repo.persist_pairing(pairing)
  end

  def persist({:error, reason}) do
    IO.puts(:stderr, "Failed to pair up: #{reason}")
    :error
  end

  def persist(:error), do: :error

  def display_result({:ok, _}) do
    IO.puts("Successfully updated pairing!")
    SecretSanta.CLI.Latest.call([])
  end

  def display_result({:error, _reason}) do
    IO.puts(:stderr, "Failed to persist pairing")
    :error
  end

  def display_result(:error), do: :error
end