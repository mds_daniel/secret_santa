defmodule SecretSanta.CLI.Wish do
  alias SecretSanta.Repo

  def cli_options do
    [
      name: "wish",
      about: "Add the given ITEM to the PERSON's wishlist",
      args: [
        person: [
          name: "person",
          value_name: "PERSON",
          help: "Person for whom the ITEM is added",
          required: true
        ],
        item: [
          name: "item",
          value_name: "ITEM",
          help: "Item to be added to the wishlist",
          required: true
        ]
      ]
    ]
  end

  def call(opts) do
    person = Keyword.fetch!(opts, :person)
    item = Keyword.fetch!(opts, :item)

    case Repo.add_to_wishlist(person, item) do
      {:ok, _} ->
        IO.puts("Added item \"#{item}\" to \"#{person}\"'s wishlist")

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to add item to wishlist")
    end
  end
end
