defmodule SecretSanta.CLI.Santa do
  alias SecretSanta.{Repo, Recipient}
  alias SecretSanta.CLI.WishList

  def cli_options do
    [
      name: "santa",
      about: "Show recipient of the gift from the given SANTA",
      args: [
        santa: [
          name: "santa",
          value_name: "SANTA",
          help: "Current santa for which the recipient will be fetched",
          required: true
        ]
      ]
    ]
  end

  def call(opts) do
    santa = Keyword.fetch!(opts, :santa)

    case Repo.recipient_for(santa) do
      {:ok, recipient} ->
        display_recipient(recipient, santa)

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to fetch santa info")
    end
  end

  def display_recipient(nil, _santa) do
    IO.puts("Pairing for secret santa not yet ready.")
  end

  def display_recipient(%Recipient{name: name, wishlist: wishlist}, santa) do
    IO.puts("As \"#{santa}\", you must give a gift to \"#{name}\"")
    WishList.display_wishlist(wishlist)
  end
end
