defmodule SecretSanta.CLI.List do
  alias SecretSanta.{Repo, NumberedList}

  def cli_options do
    [
      name: "list",
      about: "List all people enrolled for Secret Santa",
    ]
  end

  def call(_opts) do
    case Repo.list_people() do
      {:ok, people} ->
        display_people(people)

      {:error, _reason} ->
        IO.puts(:stderr, "Failed to retrieve list.")
    end
  end

  def display_people([]) do
    IO.puts("There are currently no people.")
  end

  def display_people([single_person]) do
    IO.puts("Only \"#{single_person}\" is currently on the list.")
  end

  def display_people(people) do
    IO.puts("There are #{length(people)} people on the list:")
    NumberedList.display(people)
  end
end
