defmodule SecretSanta.CLICommands do
  defmacro __using__(_opts) do
    quote do
      Module.register_attribute(__MODULE__, :cli_commands, accumulate: true, persist: false)

      import unquote(__MODULE__), only: [cli_command: 2]
      @before_compile unquote(__MODULE__)
    end
  end

  defmacro __before_compile__(env) do
    compile_run_commands(Module.get_attribute(env.module, :cli_commands))
  end

  defmacro cli_command(name, module) do
    quote bind_quoted: [name: name, module: module] do
      @cli_commands {name, module}
    end
  end

  def compile_run_commands(commands) do
    run_command_ast = run_command_definition_from(commands)
    command_list_ast = command_list_definition_from(commands)

    quote do
      unquote(run_command_ast)

      def commands do
        unquote(command_list_ast)
      end
    end
  end

  def run_command_definition_from(commands) do
    for {command, module} <- commands do
      quote do
        def run_command!(unquote(command), opts), do: unquote(module).call(opts)
      end
    end
  end

  def command_list_definition_from(commands) do
    for {command, module} <- commands do
      quote do
        {unquote(command), unquote(module).cli_options()}
      end
    end
  end
end
