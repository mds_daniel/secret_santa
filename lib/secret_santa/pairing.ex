defmodule SecretSanta.Pairing do
  def pair_up(people) do
    people
    |> Enum.shuffle()
    |> to_pairing()
  end

  def to_pairing([]), do: %{}

  def to_pairing(list) do
    first = hd(list)

    list
    |> to_pairing(first, [])
    |> Enum.into(%{})
  end

  defp to_pairing([], _first, acc), do: acc

  defp to_pairing([last], first, acc), do: [{last, first} | acc]

  defp to_pairing([a, b | rest], first, acc) do
    to_pairing([b | rest], first, [{a, b} | acc])
  end

  @doc """
  Flattens a pairing into a list of [key1, value1, key2, value2, ...]
  """
  def flatten(pairing) do
    pairing
    |> Enum.to_list()
    |> flatten([])
  end

  defp flatten([], acc), do: acc

  defp flatten([{key, value} | rest], acc) do
    flatten(rest, [key | [ value | acc]])
  end

  def from_flattened(keys_with_values) do
    keys_with_values
    |> from_flattened([])
    |> Enum.into(%{})
  end

  defp from_flattened([], acc), do: acc

  # All values must come in pairs
  defp from_flattened([single_value], _acc) do
    raise "invalid pairing structure: single value - #{single_value}"
  end

  defp from_flattened([key, value | rest], acc) do
    from_flattened(rest, [{key, value} | acc])
  end
end
