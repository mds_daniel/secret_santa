defmodule SecretSanta.Supervisor do
  use Supervisor

  @name SecretSanta.Supervisor

  def start_link(opts) do
    opts = Keyword.put_new(opts, :name, @name)
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      {Redix, name: :redix}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
