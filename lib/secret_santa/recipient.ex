defmodule SecretSanta.Recipient do
  defstruct [name: nil, wishlist: []]
end
