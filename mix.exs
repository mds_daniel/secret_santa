defmodule SecretSanta.MixProject do
  use Mix.Project

  def project do
    [
      app: :secret_santa,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      escript: escript(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {SecretSanta.Application, []},
      extra_applications: [:logger]
    ]
  end

  defp escript() do
    [main_module: SecretSanta.CLI]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:redix, "~> 0.9.0"},
      {:optimus, "~> 0.1.8"}
    ]
  end
end
